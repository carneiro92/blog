# Projet Blog PHP

Créer un blog en PHP en utilisant le framework Symfony 4.2

* Modéliser l'application en UML
    * Faire un diagramme de Use case pour l'application
    * Faire un diagramme de Classe pour représenter les entités de l'appli
* Maquetter l'application
    * Faire les wireframes de l'application
    * (Optionnel) Faire un diagramme d'activité (ou autre) pour représenter l'enchaînement des pages 
* Créer un projet Symfony et coder l'application avec le framework
    * Créer des controllers et des templates
    * Créer des classes Form (les Types)
    * Utiliser bootstrap pour les templates (comme d'hab on veut une appli responsive)
* Utiliser Mariadb / PDO pour les bases de données (pas d'ORM)
    * Créer la base de donnée du projet et les tables correspondantes aux entités avec des données de tests dedans et exporter un script pour remettre la bdd en état
    * Créer des classes d'accès au données en utilisant PDO (DAO / Repository)


    regarder twig extends