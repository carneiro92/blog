<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormEvent;


class ArticleType extends AbstractType
{

     public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
     {
         $builder->add("title", TextType::class)
         ->add("Content", TextareaType::class)
         ->add("picture1", TextType::class)
         ->add("picture2", TextType::class)
         ->add("picture3", TextType::class)
         ->add("picture4", TextType::class)
         ->add("picture5", TextType::class);
     }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}

