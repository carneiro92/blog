<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;


class User implements UserInterface
{
    public $id;
    public $name;
    public $surname;
    public $email;
    public $birthdate;
    public $admin;
    /**
     * @Assert\Length(min=5)
     */
    public $password;

    // si on utilise un constructeur dans une entité, il faut faire en sorte qu'elle soit instanciable sans propriété.
    public function __construct(string $name = null, string $surname = null, string $email = null, \DateTime $birthdate = null, string $password = null, int $id = null, string $admin = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->birthdate = $birthdate;
        $this->password = $password;
        $this->admin = $admin;
    }

    //Une méthode statique est une méthode qu'on peut utiliser directement sur une classe sans avoir besoin de créer d'instance. A la différence d'une méthode classique, on appelle une méthode statique avec avec `::` ex: User::fromSQL()
    /**
     * Construit un utilisateur à partir des données brutes de la base de données.
     * 
     * @param array Le tableau associatif contenant les résultats d'une requête SQL
     * @return User L'utilisateur construit à partir de données de la base de données
     */
    public static function fromSQL(array $rawData)
    {


        return new User(
            $rawData["name"],
            $rawData["surname"],
            $rawData["email"],
            new \DateTime($rawData["birthdate"]),
            $rawData["password"],
            $rawData["id"],
            $rawData["admin"]
        );
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {

        if ($this->admin === "ROLE_ADMIN") {
            return ["ROLE_ADMIN"];
        } else {
            return ["ROLE_USER"];
        }
    }

    public function getSalt()
    { }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    { }
}
