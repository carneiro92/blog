<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\UserRepository;

class InscriptionController extends Controller
{
    /**
     * @Route("/create-account", name="create_account")
     */
    public function index(Request $request, UserRepository $repo)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $repo->add($user);
        }

        return $this->render('Account/index.html.twig', [
            "form" => $form->createView(),
        ]);
    }
}
