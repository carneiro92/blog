<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ArticleType;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserChecker;
use App\Repository\CommentsRepository;
use App\Form\CommentsType;
use App\Entity\Comments;



class ArticleController extends AbstractController
{


    /**
     * @Route("/login", name="login")
     * @Security("has_role('ROLE_USER')")
     */
    public function login()
    {
        return $this->redirectToRoute('home');
    }
    /**
     * @Route("/user/add-article", name="add-article")
     * @Security("has_role('ROLE_USER')")
     */
    public function index(Request $request, ArticleRepository $repo)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article->datePost = new \DateTime();
            $article->author = $this->getUser()->surname;
            $repo->add($article);
            return $this->redirectToRoute("home");
        }
        return $this->render("home/add-article.html.twig", [
            "form" => $form->createView(),
            "user"=>$this->getUser()
        ]);
    }
    /**
    *  @route ("/", name="home")
    */
    public function blogMain(ArticleRepository $repo)
    {
        $articles = $repo->findAll();
        return $this->render("home/show-article.html.twig", [
            "posts" => $articles,
            "user"=>$this->getUser()
        ]);
    }
    /**
    *  @route ("/admin/delete-post{id}", name="delete-post")
    *  @Security("has_role('ROLE_ADMIN')")
    */
    public function deletePost(ArticleRepository $repo, $id)
    {
        $repo->delete($repo->find($id));
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        return $this->redirectToRoute("home");
    }
    /**
     * @Route("/show-post/{id}", name="show-post")
     */
    public function listArticle(ArticleRepository $repo, $id, CommentsRepository $repoComments,Request $request, UserRepository $user)
    {
        $user=$this->getUser();

        $article = $repo->find($id);
        $comments = new Comments();
        $comments->idArticle=$id;
        $form = $this->createForm(CommentsType::class, $comments);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comments->date = new \DateTime();
            $comments->user = $user->surname.' '.$user->name;
            
            $repoComments->add($comments);
            return $this->redirectToRoute('show-post', array('id'=>$id));
        }

        return $this->render("home/post.html.twig", [
            "post" => $article,
            "comments"=>$repoComments->findAll($id),
            "form"=>$form->createView(),
            "user"=>$this->getUser()

        ]);
    }
    /**
     * @Route("/search", name="search")
     */
    public function search(Request $request, ArticleRepository $repo)
    {
        $article = $repo->findTile($request->get('search'));
        return $this->render("home/show-article.html.twig", [
            "posts" => $article,
            "user"=>$this->getUser()
        ]);
    }
    /**
     * @Route("/admin/edit-post/{id}", name="edit-post")
     * @Security("has_role('ROLE_ADMIN')")
     */

    public function edit(int $id, Request $request, ArticleRepository $repo)
    {
        $oldEdit = $repo->find($id);
        $form = $this->createForm(ArticleType::class, $oldEdit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->datePost = new \DateTime();
            $repo->update($oldEdit);
            $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
            return $this->redirectToRoute('show-post', [
                'id' => $id,
                "user"=>$this->getUser()
            ]);
        }
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        return $this->render("admin/edit-article.html.twig", [
            "form" => $form->createView(),
            "user"=>$this->getUser()
        ]);
    }
}
