<?php

namespace App\Repository;

use Symfony\Component\HttpKernel\DataCollector\DumpDataCollector;
use App\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Comments;




class CommentsRepository
{
    public function findAll(int $id): array
    {
        $comments = [];

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM comments WHERE comments.id_article=:id");
        $query->bindValue(":id", $id);
        $query->execute();
        foreach ($query->fetchAll() as $value) {

            $comments[] = $this->sqlToComments($value);
        }
        return $comments;
    }
    public function add(Comments $comments): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("INSERT INTO comments (user,id_article,date,comments) VALUES (:user,:idArticle,:date,:comments)");
        $query->bindValue(":user", $comments->user);
        $query->bindValue(":idArticle", $comments->idArticle);
        $query->bindValue(":date", $comments->date->format('Y-m-d H:i:s:u'));
        $query->bindValue(":comments", $comments->comments);
        $query->execute();
        $comments->id = $connection->lastInsertId();
    }
    private function sqlToComments(array $value): Comments
    {
        $comments = new Comments();
        $comments->id = intval($value["id"]);
        $comments->user = $value["user"];
        $comments->idArticle = $value["id_article"];
        $comments->date = new \DateTime($value["date"]);
        $comments->comments = $value["comments"];
      
        return $comments;
    }
}