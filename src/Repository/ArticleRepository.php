<?php

namespace App\Repository;

use Symfony\Component\HttpKernel\DataCollector\DumpDataCollector;
use App\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;




class ArticleRepository
{
    public function findAll(): array
    {
        $articles = [];

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM article");
        $query->execute();
        //dump($query->fetchAll());
        foreach ($query->fetchAll() as $value) {

            $articles[] = $this->sqlToArticle($value);
        }
        return $articles;
    }
    public function add(Article $article): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("INSERT INTO article (title,Content,datePost,author,picture_1,picture_2,picture_3,picture_4,picture_5) VALUES (:title,:Content,:datePost,:author,:picture1,:picture2,:picture3,:picture4,:picture5)");
        $query->bindValue(":title", $article->title);
        $query->bindValue(":Content", $article->Content);
        $query->bindValue(":datePost", $article->datePost->format('Y-m-d H:i:s:u'));
        $query->bindValue(":author", $article->author);
        $query->bindValue(":picture1", $article->picture1);
        $query->bindValue(":picture2", $article->picture2);
        $query->bindValue(":picture3", $article->picture3);
        $query->bindValue(":picture4", $article->picture4);
        $query->bindValue(":picture5", $article->picture5);
        $query->execute();
        $article->id = $connection->lastInsertId();
    }
    public function find(Int $id): ? Article
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM article WHERE id=:id");
        $query->bindValue(":id", $id);
        $query->execute();
        if ($value = $query->fetch()) {
            return $this->sqlToArticle($value);
        }
        return null;
    }
    public function update(Article $article): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("UPDATE article SET title = :title, Content = :Content, datePost = :datePost, author = :author, picture_1 = :picture1, picture_2 = :picture2, picture_3 = :picture3, picture_4 = :picture4, picture_5 = :picture5 WHERE id = :id;");
        $query->bindValue(":id", $article->id);
        $query->bindValue(":title", $article->title);
        $query->bindValue(":Content", $article->Content);
        $query->bindValue(":author", $article->author);
        $query->bindValue(":picture1", $article->picture1);
        $query->bindValue(":picture2", $article->picture2);
        $query->bindValue(":picture3", $article->picture3);
        $query->bindValue(":picture4", $article->picture4);
        $query->bindValue(":picture5", $article->picture5);
        $query->bindValue(":datePost", $article->datePost->format('Y-m-d'));
        $query->execute();
    }
    public function delete(Article $article): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("DELETE FROM comments WHERE id_article = :id;");
        $query->bindValue(":id", $article->id);
        $query->execute();
        $query = $connection->prepare("DELETE FROM article WHERE id = :id;");
        $query->bindValue(":id", $article->id);
        $query->execute();
    }
    private function sqlToArticle(array $value): Article
    {
        $article = new Article();
        $article->id = intval($value["id"]);
        $article->title = $value["title"];
        $article->Content = $value["Content"];
        $article->datePost = new \DateTime($value["datePost"]);
        $article->author = $value["author"];
        $article->picture1 = $value["picture_1"];
        $article->picture2 = $value["picture_2"];
        $article->picture3 = $value["picture_3"];
        $article->picture4 = $value["picture_4"];
        $article->picture5 = $value["picture_5"];
        return $article;
    }
    public function findTile(String $title): ? array 
    {
        
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM article WHERE title LIKE :title");
        $query->bindValue(":title", '%'.$title.'%');
        $query->execute();
        foreach ($query->fetchAll() as $value) {
            $articles[] = $this->sqlToArticle($value);
        }
        return $articles;
    }
}

