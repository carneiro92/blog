<?php

namespace App\Repository;

use App\Entity\User;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserRepository implements UserProviderInterface
{
    // la propriété `$this->connection` contient la connexion PDO à la base de donnée.
    private $connection;

    // Lorsque symfony construira UserRepository, une nouvelle connection à la base de données sera établie
    public function __construct()
    {
        try {

            /*On fait une instance de connexion à PDO en indiquant
            le lien de la base de donnée sql et son port (host). 
            Le nom de la base de donnée (dbname) qu'on veut cibler.
            Puis le nom d'utilisateur pour s'y connecter ainsi que son
            mot de passe*/
            $this->connection = ConnectionUtil::getConnection();

        } catch (\PDOException $e) {

            dump($e);

        }
    }
    /**
     * La méthode privée `fetch()` permet de lancer une requête donnée avec ses paramètres et de directement retourner des instances de la classe User.
     * De cette façon toute la procédure de création et d'execution de la requête n'est pas répétée dans les méthodes `get()` `getAll()` et `add()`
     * 
     * @param string $query la requête SQL que l'on souhaite executer dans la base de données.
     * @param array|null $params les paramètres à affecter à notre requête ex: `[":id" => 1, ":name" => "toto"]`.
     * @return array|User|null retourne un tableau contenant des instances de User lorsqu'il y a plusieurs résultats ou une seule instance de User si il n'y a qu'un seul résultat ou null si la requête ne renvoie aucun resultat.
     */
    private function fetch(string $query, array $params = []){
        try {

            /*La méthode prepare() sur une connexion PDO va mettre une
            requête SQL en attente d'exécution à l'intérieur d'un
            objet de type PDOStatement (qu'on stock ici dans $query)*/
            $query = $this->connection->prepare($query);
            //On affecte à notre requête les valeurs passées à la méthode select si il y en a
            foreach ($params as $param => $value) {
                $query->bindValue($param, $value);
            }
            //Pour la requête soit lancée, il faut lancer la méthode execute de l'objet query
            $query->execute();

            $result = [];
            /*Pour récupérer les résultats de la requête, on peut 
            utiliser la méthode fetchAll qui renverra un tableau
            de tableau associatif. Chaque tableau associatif représentera une ligne de résultat.
            On fait une boucle sur le tableau de résultat pour
            faire en sorte de convertir chaque ligne de résultat
            brut en une instance de la classe voulue (ici User)*/
            foreach ($query->fetchAll() as $row) {
                // on appelle la méthode statique `fromSQL()` pour pour créer des instances de `User` à partir des données brutes onvoyées par la base de données.
                $result[] = User::fromSQL($row);
            }
            //si il n'y a qu'un seul utilisateur ou moins, on le retourne directement ...
            if (count($result) <= 1) {
                return $result[0];
            }
            //... dans tout les autres cas, on retourne le tableau d'utilisateurs.
            return $result;

        } catch (\PDOException $e) {
            dump($e);
        }

    }
    /**
     * La méthode `add()` permet d'insérer un nouvel utilisateur dans la base de données, elle retourne l'utilisateur fraîchement créé.
     * 
     * @param User $user L'utilisateur à insérer dans la base de données.
     * @return User L'utilisateur fraîchement créé.
     */
    public function add(User $user)
    {
        // on appelle la méthode privée `fetch()` que l'on à créée ci-dessus pour inserer notre utilisateur.
        $this->fetch("INSERT INTO user (name, surname, email, birthdate, password, admin) VALUES (:name, :surname, :email, :birthdate, :password ,:admin)",[
            ":name" => $user->name,
            ":surname" => $user->surname,
            ":email" => $user->email,
            ":birthdate" => $user->birthdate->format('Y-m-d H:i:s:u'),
            ":admin" => 'ROLE_USER',
            ":password" => $user->password
        ]);
        // lorsque l'utilisateur est créé dans base de données,
        // on affecte la valeur de l'id qui vient d'être inséré à l'instance de `User` ...
        $user->id = intval($this->connection->lastInsertId());
        // ... puis on retourne l'utilisateur
        return $user;
    }

    /**
     * La méthode `get()` permet de sélectionner un utilisateur dans la base de données à partir d'un id.
     * 
     * @param int $id L'id de l'utilisateur à selectionner.
     * @return User|null L'utilisateur correspondant à l'id.
     */
    public function get(int $id)
    {
        // on appelle la méthode privée `fetch()` pour selectionner notre utilisateur.
        return $this->fetch("SELECT * FROM user WHERE :id", [":id" => $id]);
    }

    /**
     * La méthode `getAll()` permet de sélectionner tout les utilisateurs dans la basede données.
     * 
     * @param int $id L'id de l'utilisateur à selectionner.
     * @return array|User|null L'utilisateur correspondant à l'id.
     */
    public function getAll()
    {
        // on appelle la méthode privée `fetch()` pour selectionner tout les utilisateurs.
        return $this->fetch("SELECT * FROM user");
    }
    public function loadUserByUsername($username)
    {
        $user = $this->fetch("SELECT * FROM user WHERE email=:email", [
            ":email" => $username
        ]);
        if($user instanceof User) {
            return $user;
        }
        throw new UsernameNotFoundException("User doesn't exist");
    }

    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
