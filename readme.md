# Car Blog

In this blog the theme is the cars and the idea is to post some cool car news and content. 



# Interest

The main propose of this project was to practice Symfony 4 and Mysql



## Built With

* [Symfony](https://symfony.com/) - The web framework used
* [MariaDB](https://go.mariadb.com/search-download-MariaDB-server.html?utm_source=google&utm_medium=ppc&utm_campaign=MKG-Search-Google-Branded-EMEA-bd&gclid=CjwKCAiA2fjjBRAjEiwAuewS_cJpS5jaY1mdDvnCd4uiGNVvNtqwVNcDyELlyDFdYzpDKPe-s6niPxoCNGwQAvD_BwE) - Database

## Authors

* **Jorge Carneiro** - *Project Maker* - [Carneiro92](https://gitlab.com/carneiro92)

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc






<h3>MainPage</h3>

![Main](Img/Capture du 2019-03-05 14-49-09.png)

<h3>AddContent</h1>

![AddContent](Img/Capture du 2019-03-05 14-47-33.png)

<h3>View Post</h3>

![ViewPost](Img/Capture du 2019-03-05 14-55-21.png)